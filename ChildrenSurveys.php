<?php

/**
 * Get the children surveys by plugin
 * This file is part of RelatedSurveyManagement plugin
 * 
 * @license AGPL v3
 * @since 0.10.0 : add getSurveyidOfDescendants and getChildsSurveys function
 * @since 0.8.5
 *
 */

namespace RelatedSurveyManagement;

use Yii;
use CDbCriteria;
use PDO;
use CException;
use Survey;
use Question;
use QuestionAttribute;

class ChildrenSurveys
{
    /**
     * @var integer survey id
     */
    public $surveyId;

    /**
     * @var null|integer[] the related survey id
     */
    private $aChildrensSurveys;

    /**
     * @var null|string[] plugin used by qid
     */
    private $aQidsPlugin = array();

    /**
     * @var null|array[] qid for key, plugin and information for value
     */
    private $aQidsRestrictions = array();

    /**
     * The restriction by survey
     * @var array[]
     */
    private $aSurveysRestriction;

    /**
     * Errors by qids
     * @var array[]
     */
    private $aQidsErrors = array();
    /**
     * Warnings by qids
     * @var array[]
     */
    private $aQidsWarnings = array();

    /**
     * constructor
     * @param integer survey id
     * @throw Exception
     */
    public function __construct($surveyId)
    {
        if (!Utilities::isSurveyExist($surveyId)) {
            throw new Exception(404, 'Invalid survey id');
        }
        $this->surveyId = $surveyId;
        $this->setChildrensSurveys();
    }

    /**
     * Return the surveys and is restriction for a survey by a questin
     * @param integer $qid
     * @return array : restriction done for current qid
     */
    public function getChildrenSurveyRestriction($qid)
    {
        if (empty($this->aQidsPlugin[$qid])) {
            throw new CException(500, sprintf("No plugin for question %s", $qid));
        }
        $aSurveyRelatedInformation = array(
            'id' => false,
            'token' => false,
            'other' => array()
        );
        if (!isset($this->aQidsWarnings[$qid])) {
            $this->aQidsWarnings[$qid] = array();
        }
        if (!isset($this->aQidsErrors[$qid])) {
            $this->aQidsErrors[$qid] = array();
        }
        $plugin = $this->aQidsPlugin[$qid];
        $availableRelatedPlugins = Utilities::getAvailableRelatedPlugins();
        $aAttributes = $availableRelatedPlugins[$plugin];
        $extraSurveyId = Utilities::getQuestionAttribute($qid, $aAttributes['surveyid']);
        /* check if exist ? */
        $oExtraSurveyQuestionCode = array_flip(\getQuestionInformation\helpers\surveyCodeHelper::getAllQuestions($extraSurveyId));

        /* id relation */
        $sridLink = false;
        $sridLinkCode = Utilities::getQuestionAttribute($qid, $aAttributes['question-srid']);
        $useId = true;
        if ($aAttributes['question-useid']) {
            $useId = boolval(Utilities::getQuestionAttribute($qid, $aAttributes['question-useid'], true));
        }
        if ($sridLinkCode) {
            /* Find it in oExtraSurveyQuestionCode */
            if (isset($oExtraSurveyQuestionCode[$sridLinkCode])) {
                $aSurveyRelatedInformation['id'] = $sridLinkCode;
            } elseif ($useId) {
                $this->aQidsWarnings[$qid][] = sprintf(Utilities::translate("Invalid question code %s for survey %s for id."), $sridLinkCode, $extraSurveyId);
            }
        }

        /* token */
        $tokenLink = Utilities::getQuestionAttribute($qid, $aAttributes['token-usage'], 'token');
        if ($tokenLink) { // ===""
            $aSurveyRelatedInformation['token'] = $tokenLink;
        }
        /* others */
        $othersLink = trim(Utilities::getQuestionAttribute($qid, $aAttributes['questions-others'], null));
        if ($othersLink) {
            $fixedOtherLinks = $this->getFixedOtherField($qid, $othersLink, $extraSurveyId);
            $aSurveyRelatedInformation['other'] = $fixedOtherLinks;
        }
        return $aSurveyRelatedInformation;
    }

    /**
     * get all childrens qid with related plugins
     */
    public function getQidsPlugin()
    {
        return $this->aQidsPlugin;
    }

    /**
     * get all childrens surveys
     */
    public function getChildrensSurveys()
    {
        $this->setChildrensSurveys();
        return $this->aChildrensSurveys;
    }

    /**
     * Get restriction
     * @return array[]
     */
    public function getQidsRestrictions()
    {
        return $this->aQidsRestrictions;
    }
    /**
     * get errors
     * $var integer $qid, if set force find it, else return current errors array
     * return array
     */
    public function getQidsErrors($qid = null)
    {
        if ($qid) {
            if (empty($this->aQidsPlugin[$qid])) {
                throw new CException(500, sprintf("No plugin for question %s", $qid));
            }
            if (!isset($this->aQidsErrors[$qid])) {
                $this->getChildrenSurveyRestriction($qid);
            }
            return $this->aQidsErrors[$qid];
        }
        return $this->aQidsErrors;
    }

    /**
     * Get warnings
     * $var integer $qid, if set force find it, else return current warnings array
     * return array
     */
    public function getQidsWarnings($qid = null)
    {
        if ($qid) {
            if (empty($this->aQidsPlugin[$qid])) {
                throw new CException(500, sprintf("No plugin for question %s", $qid));
            }
            if (!isset($this->aQidsErrors[$qid])) {
                $this->getChildrenSurveyRestriction($qid);
            }
            return $this->aQidsWarnings[$qid];
        }
        return $this->aQidsWarnings;
    }

    /**
     * Set id of question for key and surveyid for value
     * @return integer[]
     */
    private function setChildrensSurveys()
    {
        if (is_array($this->aChildrensSurveys)) {
            return;
        }
        $this->aChildrensSurveys = array();

        $availablePlugins = Utilities::getAvailableRelatedPlugins();
        $surveyAttributes = array_map(function ($availablePlugin) {
            return $availablePlugin['surveyid'];
        }, $availablePlugins);
        $oSurvey = Survey::model()->findByPk($this->surveyId);
        $surveyId = $this->surveyId;
        $language = $oSurvey->language;
        $questionTable = Question::model()->tableName();
        if (intval(App()->getConfig('versionnumber')) > 3) {
            $command = Yii::app()->db->createCommand()
                ->select("qid,{{groups}}.group_order, {{questions}}.question_order")
                ->from($questionTable)
                ->where("({{questions}}.sid = :sid AND {{questions}}.parent_qid = 0) AND ({{questions}}.type = 'X' OR {{questions}}.type = 'T')")
                ->join('{{groups}}', "{{groups}}.gid = {{questions}}.gid")
                ->order("{{groups}}.group_order asc, {{questions}}.question_order asc")
                ->bindParam(":sid", $surveyId, PDO::PARAM_INT);
        } else {
            $command = Yii::app()->db->createCommand()
                ->select("qid,{{questions}}.language as language,{{groups}}.group_order, {{questions}}.question_order")
                ->from($questionTable)
                ->where("({{questions}}.sid = :sid AND {{questions}}.language = :language AND {{questions}}.parent_qid = 0) AND ({{questions}}.type = 'X' OR {{questions}}.type = 'T')")
                ->join('{{groups}}', "{{groups}}.gid = {{questions}}.gid  AND {{questions}}.language = {{groups}}.language")
                ->order("{{groups}}.group_order asc, {{questions}}.question_order asc")
                ->bindParam(":sid", $surveyId, PDO::PARAM_INT)
                ->bindParam(":language", $language, PDO::PARAM_STR);
        }
        $aPossibleQuestions = $command->query()->readAll();
        foreach ($aPossibleQuestions as $aQuestion) {
            $criteria = new CDbCriteria();
            $criteria->condition = "TRIM(value) != ''";
            $criteria->compare('qid', $aQuestion['qid']);
            $criteria->addInCondition('attribute', $surveyAttributes);
            $oQuestionAttributeSurvey = QuestionAttribute::model()->find($criteria);
            if ($oQuestionAttributeSurvey) {
                $extraSurveyId = trim($oQuestionAttributeSurvey->value);
                if ($extraSurveyId) {
                    $this->aQidsPlugin[$aQuestion['qid']] = $oQuestionAttributeSurvey->getAttribute('attribute');
                    $this->aQidsErrors[$aQuestion['qid']] = array();
                    $this->aQidsWarnings[$aQuestion['qid']] = array();
                    if (!Utilities::isSurveyExist($extraSurveyId)) {
                        $this->aQidsErrors[$aQuestion['qid']][] = sprintf(Utilities::translate("Invalid survey %s"), $extraSurveyId);
                        continue;
                    }
                    /* @todo : Check validity of survey */
                    $this->aChildrensSurveys[$aQuestion['qid']] = $extraSurveyId;
                } elseif ($extraSurveyId === "0") {
                    $this->aQidsPlugin[$aQuestion['qid']] = $oQuestionAttributeSurvey->getAttribute('attribute');
                    $this->aQidsErrors[$aQuestion['qid']] = array();
                    $this->aQidsWarnings[$aQuestion['qid']] = array();
                    $this->aQidsErrors[$aQuestion['qid']][] = Utilities::translate("No survey choosen.");
                }
            }
        }
    }

    /**
     * Return array with $key and value for quis
     * @param integer $qid related
     * @param string : the opther field
     * @param integer $extraSurveyId related surey
     *
     * return array : [column=>[fixeds =>[], columns =[]] ]
     */

    private function getFixedOtherField($qid, $otherField, $extraSurveyId)
    {
        $aOtherFieldsLines = preg_split('/\r\n|\r|\n/', $otherField, -1, PREG_SPLIT_NO_EMPTY);
        $aOtherFields = array();
        $oExtraSurveyQuestionCode = array_flip(\getQuestionInformation\helpers\surveyCodeHelper::getAllQuestions($extraSurveyId));
        $oExtraSurveyQuestionCode['token'] = 'token'; // Can use token
        foreach ($aOtherFieldsLines as $otherFieldLine) {
            if (!strpos($otherFieldLine, ":")) {
                continue; // Invalid line
            }
            $key = substr($otherFieldLine, 0, strpos($otherFieldLine, ":"));
            $value = substr($otherFieldLine, strpos($otherFieldLine, ":") + 1);
            if (isset($oExtraSurveyQuestionCode[$key]) && $value) {
                $relatedColumn = $oExtraSurveyQuestionCode[$key];
                $aOtherFields[$key] = $value;
            } else {
                $this->aQidsWarnings[$qid][] = sprintf(Utilities::translate("Invalid question code %s in survey %s"), $key, $extraSurveyId);
            }
        }
        return $aOtherFields;
    }

    /**
     * Get the childs surey in a tree way
     * @param $surveyId
     * @return array[]
     */
    public static function getChildsOfSurvey($surveyId)
    {
        $oChildrenSurveys = new self($surveyId);
        $aChildrensSurveys = $oChildrenSurveys->getChildrensSurveys();
        $aSurveyTree = array();
        foreach ($aChildrensSurveys as $childSurveyId) {
            $oSurvey = Survey::model()->findByPk($childSurveyId);
            if ($oSurvey) {
                $aSurveyTree[$childSurveyId] = array(
                    'id' => $childSurveyId,
                    'title' => $oSurvey->getLocalizedTitle(),
                    'childs' => self::getChildsOfSurvey($childSurveyId),
                );
            } else {
                // Done before ?
            }
        }
        return $aSurveyTree;
    }

    /**
     * Get the childs survey as an array of id
     * @param $surveyId
     * @return array[]
     */
    public static function getSurveyidOfDescendants($surveyId)
    {
        $oChildrenSurveys = new self($surveyId);
        $aChildrensSurveys = $oChildrenSurveys->getChildrensSurveys();

        $aSurveysId = array();
        foreach ($aChildrensSurveys as $childSurveyId) {
            if (!isset($aSurveysId[$childSurveyId])) {
                $aSurveysId = $aSurveysId + self::getSurveyidOfDescendants($childSurveyId);
            }
            $aSurveysId[$childSurveyId] = $childSurveyId;
        }
        return $aSurveysId;
    }

    /**
     * Get the childs survey
     * @param $surveyId
     * @return array[]
     */
    public static function getChildsSurveys($surveyId)
    {
        $oChildrenSurveys = new self($surveyId);
        return $oChildrenSurveys->getChildrensSurveys();
    }
}
