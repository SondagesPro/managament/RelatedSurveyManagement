<?php

/**
 * Tools for related survey
 * This file is part of RelatedSurveyManagement plugin
 * @license AGPL v3
 * @since 0.13.0
 */

namespace RelatedSurveyManagement;

use Yii;
use CDbCriteria;
use Survey;
use Response;

class RelatedSurveysHelper
{
    /**
     * @var self
     */
    private static $instance;
    /**
     * @var integer survey id
     */
    private $surveyId;

    /**
     * @var array filter to be apply
     */
    private static $columnFilterBySurveyId = [];

    /**
     * constructor
     * @param integer survey id
     * @throws Exception
     */
    public function __construct($surveyId)
    {
        if (!Utilities::isSurveyExist($surveyId)) {
            throw new Exception(404, 'Invalid survey id');
        }
        $this->surveyId = $surveyId;
    }

    /**
     * Get the singleton
     * @todo : allow keep multiple instance
     * return self
     */
    public static function getInstance($surveyId)
    {
        self::$instance = new self($surveyId);
        return self::$instance;
    }
    /**
     * get filter for specific survey
     * @return [] column=>value for criteria
     */
    private function getDeleteColumnValue()
    {
        if (isset(self::$columnFilterBySurveyId[$this->surveyId])) {
            return self::$columnFilterBySurveyId[$this->surveyId];
        }
        $settings = Settings::getInstance();
        $deleteAction = $settings->getSetting($this->surveyId, 'deleteAction');
        if (empty($deleteAction)) {
            self::$columnFilterBySurveyId[$this->surveyId] = [];
            return [];
        }
        $deleteActionColumn = trim($settings->getSetting($this->surveyId, 'deleteActionColumn'));
        $deleteActionValue = trim($settings->getSetting($this->surveyId, 'deleteActionValue'));
        if (empty($deleteActionColumn)) {
            self::$columnFilterBySurveyId[$this->surveyId] = [];
            return [];
        }
        $deleteActionColumn = \getQuestionInformation\helpers\surveyCodeHelper::getColumnName($this->surveyId, $deleteActionColumn);
        if ($deleteActionColumn) {
            self::$columnFilterBySurveyId[$this->surveyId] = [
                'column' => $deleteActionColumn,
                'value' => $deleteActionValue
            ];
            return self::$columnFilterBySurveyId[$this->surveyId];
        }
        self::$columnFilterBySurveyId[$this->surveyId] = [];
        return [];
    }

    /**
     * Add the filter criteria
     * @var \CDbCriteria
     * @return \CDbCriteria
     */
    public function addFilterDeletedCriteria(CDbCriteria $criteria): CDbCriteria
    {
        $deleteColumnValue = $this->getDeleteColumnValue();
        if (empty($deleteColumnValue)) {
            return $criteria;
        }
        if ($deleteColumnValue['value'] !== '') {
            $quotedColumn = App()->db->quoteColumnName($deleteColumnValue['column']);
            $addCriteria = new CDbCriteria();
            $addCriteria->condition = $quotedColumn . ' is NULL';
            $addCriteria->addNotInCondition($quotedColumn, [$deleteColumnValue['value']], 'OR');
            $criteria->mergeWith($addCriteria);
        } else {
            /* check if work for date and numerci value */
            $quotedColumn = App()->db->quoteColumnName($deleteColumnValue['column']);
            $addCriteria = new CDbCriteria();
            $addCriteria->condition = "$quotedColumn is NOT NULL AND $quotedColumn != ''";
            $criteria->mergeWith($addCriteria);
        }
        return $criteria;
    }

    /**
     * Delete response according to settings
     * @var \CDbCriteria $criteria
     * @var boolean
     * @return integer : number of response deleted (DB dependant)
     */
    public function deleteResponse(CDbCriteria $criteria, $toChilds = true): int
    {
        $deleteColumnValue = $this->getDeleteColumnValue();
        /* find each child to be deleted too */
        $settings = Settings::getInstance();
        $childsLink = $settings->getSetting($this->surveyId, 'childsLink');
        $surveyId = $this->surveyId;
        if (!empty($childsLink) && version_compare(App()->getConfig('getQuestionInformationAPI'), "3.1.0", ">=")) {
            foreach ($childsLink as $childId => $childLink) {
                if (!tableExists('survey_' . $childId)) {
                    continue;
                }
                if (!empty($childLink['deleteWith']) && !empty($childLink['current']) && !empty($childLink['child'])) {
                    $current = \getQuestionInformation\helpers\surveyCodeHelper::getColumnName($this->surveyId, $childLink['current']);
                    $child = \getQuestionInformation\helpers\surveyCodeHelper::getColumnName($childId, $childLink['child']);
                    if (empty($current) || empty($child)) {
                            // @todo : log it as error
                            continue;
                    }
                    $getValueCriteria = new CDbCriteria();
                    $getValueCriteria->distinct = true;
                    $getValueCriteria->group = App()->getDb()->quoteColumnName($current);
                    $getValueCriteria->mergeWith($criteria);
                    $getValueCriteria->select = App()->getDb()->quoteColumnName($current);
                    $getValues = Response::model($this->surveyId)->findAll($getValueCriteria);
                    $aValues = \CHtml::listData($getValues, $current, $current);
                    $childCriteria = new CDbCriteria();
                    $childCriteria->addInCondition(App()->getDb()->quoteColumnName($child), $aValues);
                    $childRelatedSurveysHelper = new self($childId);
                    $childRelatedSurveysHelper->deleteResponse($childCriteria);
                }
            }
        }
        if (empty($deleteColumnValue)) {
            return Response::model($this->surveyId)->deleteAll($criteria);
        }
        if (Response::model($this->surveyId)->count($criteria)) {
            return Response::model($this->surveyId)->updateAll(
                [$deleteColumnValue['column'] => $deleteColumnValue['value']],
                $criteria
            );
        }
        return 0;
    }

    /**
     * Un Delete response according to settings
     * @var \CDbCriteria $criteria
     * @var toChilds boolean
     * @return null|integer : number of response un deleted (DB dependant), null if not was set
     */
    public function undeleteResponse(CDbCriteria $criteria, $toChilds = true): ?int
    {
        $deleteColumnValue = $this->getDeleteColumnValue();
        /* find each child to be deleted too */
        $settings = Settings::getInstance();
        $childsLink = $settings->getSetting($this->surveyId, 'childsLink');
        $surveyId = $this->surveyId;
        if (!empty($childsLink) && version_compare(App()->getConfig('getQuestionInformationAPI'), "3.1.0", ">=")) {
            foreach ($childsLink as $childId => $childLink) {
                if (!tableExists('survey_' . $childId)) {
                    continue;
                }
                if (!empty($childLink['deleteWith']) && !empty($childLink['current']) && !empty($childLink['child'])) {
                    $current = \getQuestionInformation\helpers\surveyCodeHelper::getColumnName($this->surveyId, $childLink['current']);
                    $child = \getQuestionInformation\helpers\surveyCodeHelper::getColumnName($childId, $childLink['child']);
                    if (empty($current) || empty($child)) {
                            // @todo : log it as error
                            continue;
                    }
                    $getValueCriteria = new CDbCriteria();
                    $getValueCriteria->distinct = true;
                    $getValueCriteria->group = App()->getDb()->quoteColumnName($current);
                    $getValueCriteria->mergeWith($criteria);
                    $getValueCriteria->select = App()->getDb()->quoteColumnName($current);
                    $getValues = Response::model($this->surveyId)->findAll($getValueCriteria);
                    $aValues = \CHtml::listData($getValues, $current, $current);
                    $childCriteria = new CDbCriteria();
                    $childCriteria->addInCondition(App()->getDb()->quoteColumnName($child), $aValues);
                    $childRelatedSurveysHelper = new self($childId);
                    $childRelatedSurveysHelper->undeleteResponse($childCriteria);
                }
            }
        }
        if (empty($deleteColumnValue)) {
            return null;
        }
        if (Response::model($this->surveyId)->count($criteria)) {
            return Response::model($this->surveyId)->updateAll(
                [$deleteColumnValue['column'] => new \CDbExpression("NULL")],
                $criteria
            );
        }
        return 0;
    }
}
