<?php

/**
 * Some Utilities
 * This file is part of RelatedSurveyManagement plugin
 *
 * @license AGPL v3
 * @since 0.2.0
 *
 */

namespace RelatedSurveyManagement;

use Yii;
use Survey;
use Plugin;
use PluginSetting;
use QuestionAttribute;
use CDBCriteria;
use CHtml;

class Utilities
{
    /* @var string give information for other plugin of Plugin API version */
    const API = "0.13.0";

    /**
     * @var null|array[]
     * Array of related plugins with plugin name as key
     * current plugin value are attribute used for:
     * - question-srid : quetion used for srid
     * - question-useid : find only srid, not set mean true
     * - questions-others : used for other relation : allow to find uniqueId
     * - token-usage : attribute for way of using token : no|token|group , default is token. If not set : token. (used in spreadsheetSurveyTokenUsage)
     * - haverelated : unsure of usage
     * - title : attribute used for title of the related survey, used in TokenUserManagement for the title in the list
     */
    private static $availableRelatedPlugins;

    /**
     * Return the related plugin
     * @todo add this to plugin via a function in plugins or
     * Any other solution ? New event ?
     */
    public static function getAvailableRelatedPlugins()
    {
        if (is_null(self::$availableRelatedPlugins)) {
            self::$availableRelatedPlugins = array(
                /* questionExtraSurvey plugin */
                'extraSurvey' => array(
                    'surveyid' => 'extraSurvey',
                    'question-srid' => 'extraSurveyQuestionLink',
                    'question-useid' => 'extraSurveyQuestionLinkUse',
                    'questions-others' => 'extraSurveyOtherField',
                    'token-usage' => 'extraSurveyTokenUsage',
                    'haverelated' => true,
                    'title' => 'extraSurveyQuestion'
                ),
                'spreadsheetSurvey' => array(
                    'surveyid' => 'spreadsheetSurvey',
                    'question-srid' => 'spreadsheetSurveyQuestionLink',
                    'question-useid' => null,
                    'questions-others' => 'spreadsheetSurveyOtherField',
                    'token-usage' => 'spreadsheetSurveyTokenUsage',
                    'haverelated' => false,
                    'title' => null
                ),
            );
        }
        return self::$availableRelatedPlugins;
    }

    /**
     * Get a DB setting from a plugin
     * @param integer plugin id
     * @param integer survey id
     * @param string setting name
     * @param mixed default if not set
     * @return mixed
     */
    public static function getSurveyPluginSetting($pluginName, $surveyId, $sSetting, $default = null)
    {

        $oPlugin = \Plugin::model()->find(
            "name = :name",
            array(":name" => $pluginName)
        );
        if (!$oPlugin || !$oPlugin->active) {
            return $default;
        }
        $oSetting = \PluginSetting::model()->find(
            'plugin_id = :pluginid AND ' . \App()->getDb()->quoteColumnName('key') . ' = :key AND model = :model AND model_id = :surveyid',
            array(
                ':pluginid' => $oPlugin->id,
                ':key' => $sSetting,
                ':model' => 'Survey',
                ':surveyid' => $surveyId,
            )
        );
        if (!empty($oSetting)) {
            return json_decode($oSetting->value);
        }
        return $default;
    }

    /**
     * Get a question attaribute
     * @param integer qid
     * @param string attribute
     * @param mixed default
     * @return mixed
     */
    public static function getQuestionAttribute($qid, $attribute, $default = null)
    {
        $oQuestionAttribute = \QuestionAttribute::model()->find(
            'qid = :qid AND attribute = :attribute',
            array(
                ':qid' => $qid,
                ':attribute' => $attribute,
            )
        );
        if (!empty($oQuestionAttribute)) {
            return $oQuestionAttribute->value;
        }
        return $default;
    }

    /**
     * Return a translated message
     * @param string $string
     * @return string
     */
    public static function translate($string, $sLanguage = null)
    {
        return \Yii::t(
            '',
            $string,
            array(),
            'RelatedSurveyManagementMessages',
            $sLanguage
        );
    }

    /**
     * Check validity of a survey
     * @param integer $surveyId
     * @return : boolean
     */
    public static function isSurveyExist($surveyId)
    {
        $oSurvey = \Survey::model()->findByPk($surveyId);
        if (empty($oSurvey)) {
            return false;
        }
        return true;
    }

    /**
     * return lsutdata od Survey list
     * @param integer[] restriction to this survey
     * @param integer current surveyId included at top (anw without Permission check)
     * @return : array as Chtml::listData
     */
    public static function getSurveysListData($restrictSurveyIds = [], $currentSurveyId = null)
    {
        $criteria = new CDBCriteria();
        $criteria->select = 't.sid';
        $criteria->with = ["defaultlanguage" => ["select" => "surveyls_title"]];
        $criteria->order = 'surveyls_title';
        if (!empty($restrictSurveyIds)) {
            $criteria->addInCondition('sid', $restrictSurveyIds);
        }
        $aWholeSurveys = Survey::model()
            ->permission(Yii::app()->user->getId())
            ->with('defaultlanguage')
            ->findAll($criteria);
        $aWholeSurveysListData = CHtml::listData($aWholeSurveys, 'sid', function ($oSurvey) {
            if (empty($oSurvey->defaultlanguage)) {
                return $oSurvey->sid;
            }
            return "[" . $oSurvey->sid . "] " . \viewHelper::flatEllipsizeText($oSurvey->defaultlanguage->surveyls_title, true, 80);
        });
        if ($currentSurveyId) {
            $criteria = new CDBCriteria();
            $criteria->select = 't.sid';
            $criteria->with = ["defaultlanguage" => ["select" => "surveyls_title"]];
            $oIncludedSurvey = Survey::model()
                ->with('defaultlanguage')
                ->findByPk($currentSurveyId);
            if ($oIncludedSurvey) {
                $title = $oIncludedSurvey->sid;
                if (!empty($oIncludedSurvey->defaultlanguage)) {
                    $title = "[" . $oIncludedSurvey->sid . "] " . \viewHelper::flatEllipsizeText($oIncludedSurvey->defaultlanguage->surveyls_title, true, 80);
                }
                $aWholeSurveysListData = [$oIncludedSurvey->sid => $title] + $aWholeSurveysListData;
            }
        }

        return $aWholeSurveysListData;
    }
}
