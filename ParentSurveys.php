<?php

/**
 * Get the availabmle related export
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2020-2023 Denis Chenu <http://www.sondages.pro>
 * @license AGPL v3
 * @version 0.8.4
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

namespace RelatedSurveyManagement;

use Yii;
use CDbCriteria;
use PDO;
use CException;
use Survey;
use Question;
use QuestionAttribute;

class ParentSurveys
{
    /**
     * Singleton
     * @var self
     */
    private static $instance = null;

    /**
     * @var integer|null survey id
     */
    public $surveyId;

    /**
     * @var null|array array of parent survey by qid
     */
    private $aParentSurveys;

    /**
     * @var null|integer|falsefinalancestrysurvey
     */
    private $ancestrySurvey;

    /**
     * Warnings by qids
     * @var array[]
     */
    private $aQidsWarnings = array();

    /**
     * constructor
     * @param integer survey id
     * @throw Exception
     */
    public function __construct($surveyId)
    {
        if (!Utilities::isSurveyExist($surveyId)) {
            throw new Exception(404, 'Invalid survey id');
        }
        $this->surveyId = $surveyId;
    }

    /**
     * To get a singleton : some part are not needed to do X times
     * @param integer survey id
     * return self
     */
    public static function getInstance($surveyId)
    {
        if ((null === self::$instance) || ($surveyId !== self::$instance->surveyId)) {
            self::$instance = new self($surveyId);
        }
        return self::$instance;
    }

    /**
     * Return the parent survey for key and qid for value
     * @return int[]);
     */
    public function getParentSurveys()
    {
        if (is_array($this->aParentSurveys)) {
            return $this->aParentSurveys;
        }
        $this->aParentSurveys = array();
        $availableRelatedPlugins = Utilities::getAvailableRelatedPlugins();
        $surveyIdByPlugin = array_map(function ($aPlugin) {
            return $aPlugin['surveyid'];
        }, $availableRelatedPlugins);
        $criteria = new CDbCriteria();
        $criteria->compare('value', $this->surveyId);
        $criteria->addInCondition('attribute', $surveyIdByPlugin);
        if (intval(App()->getConfig('versionnumber')) > 3) {
            $aoQuestions = Question::model()->with('questionattributes')->findAll($criteria);
        } else {
            $aoQuestions = Question::model()->with('questionAttributes')->findAll($criteria);
        }
        foreach ($aoQuestions as $oQuestion) {
            $this->aParentSurveys[$oQuestion->qid] = $oQuestion->sid;
        }
        return $this->aParentSurveys;
    }

    /**
     * Return the final ancestry if exist
     * interger if exist
     * null if not ancestry
     * false if it's not single
     * @return int|null|boolean;
     */
    public function getFinalAncestrySurvey()
    {
        if (!is_null($this->ancestrySurvey)) {
            return $this->ancestrySurvey;
        }
        $ancestrySurveys = array_unique($this->getParentSurveys());
        if (count($ancestrySurveys) == 0) {
            /* Keep null for API compat */
            return null;
        }
        while (count($ancestrySurveys) == 1) {
            $ancestrySurvey = reset($ancestrySurveys);
            $oNextParentSurvey = new self($ancestrySurvey);
            $nextParentSurveys = $oNextParentSurvey->getParentSurveys();
            if (empty($nextParentSurveys)) {
                break;
            }
            $ancestrySurveys = $nextParentSurveys;
        }
        /* @todo : found a way for such situation : one can be ancestry of other one */
        if (count($ancestrySurveys) > 1) {
            $this->ancestrySurvey = false;
            return $this->ancestrySurvey;
        }
        $this->ancestrySurvey = reset($ancestrySurveys);
        return $this->ancestrySurvey;
    }

    /**
     * Return the restriction done with current surevy for tghis question
     * @see RelatedSurveyManagement\ChildrenSurveys\getChildrenSurveyRestriction
     * @todo : move to Utilities
     * @return null|array;
     */
    public function getSurveyRestriction($qid)
    {
        $availableRelatedPlugins = Utilities::getAvailableRelatedPlugins();
        $surveyIdByPlugin = array_map(function ($aPlugin) {
            return $aPlugin['surveyid'];
        }, $availableRelatedPlugins);
        $criteria = new CDbCriteria();
        $criteria->compare('value', $this->surveyId);
        $criteria->compare('qid', $qid);
        $criteria->addInCondition('attribute', $surveyIdByPlugin);
        $oQuestionAttribute = \QuestionAttribute::model()->find($criteria);
        if (!$oQuestionAttribute) {
            return null;
        }
        if (!isset($this->aQidsWarnings[$qid])) {
            $this->aQidsWarnings[$qid] = array();
        }
        $oThisSurveyQuestionCode = array_flip(\getQuestionInformation\helpers\surveyCodeHelper::getAllQuestions($this->surveyId));

        $aRestrictions = array();

        $aAttributes = $availableRelatedPlugins[$oQuestionAttribute->attribute];
        /* id relation */
        $sridLink = false;
        $sridLinkCode = null;
        $useId = false;
        if ($aAttributes['question-srid']) {
            $sridLinkCode = Utilities::getQuestionAttribute($qid, $aAttributes['question-srid']);
            $useId = true;
        }
        $aRestrictions['id'] = false;
        if ($aAttributes['question-useid']) {
            $useId = boolval(Utilities::getQuestionAttribute($qid, $aAttributes['question-useid'], true));
        }
        if ($sridLinkCode) {
            if (isset($oThisSurveyQuestionCode[$sridLinkCode])) {
                $aRestrictions['id'] = $sridLinkCode;
            } elseif ($useId) {
                $this->aQidsWarnings[$qid][] = sprintf(Utilities::translate("Invalid question code %s for survey %s for id."), $sridLinkCode, $extraSurveyId);
            }
        }
        /* token */
        $aRestrictions['token'] = false;
        $tokenLink = Utilities::getQuestionAttribute($qid, $aAttributes['token-usage'], 'token');
        if ($tokenLink) { // ===""
            $aRestrictions['token'] = $tokenLink;
        }

        /* others */
        $aRestrictions['other'] = array();
        $othersLink = trim(Utilities::getQuestionAttribute($qid, $aAttributes['questions-others'], null));
        if ($othersLink) {
            $fixedOtherLinks = $this->getFixedOtherField($qid, $othersLink, $this->surveyId);
            $aRestrictions['other'] = $fixedOtherLinks;
        }
        return $aRestrictions;
    }

    /**
     * Return array with $key and value for quis
     * @param integer $qid related
     * @param string : the opther field
     * @param integer $extraSurveyId related surey
     *
     * return array : [column=>[fixeds =>[], columns =[]] ]
     */
    private function getFixedOtherField($qid, $otherField, $extraSurveyId)
    {
        $aOtherFieldsLines = preg_split('/\r\n|\r|\n/', $otherField, -1, PREG_SPLIT_NO_EMPTY);
        $aOtherFields = array();
        $oExtraSurveyQuestionCode = array_flip(\getQuestionInformation\helpers\surveyCodeHelper::getAllQuestions($extraSurveyId));
        $oExtraSurveyQuestionCode['token'] = 'token'; // Can use token
        foreach ($aOtherFieldsLines as $otherFieldLine) {
            if (!strpos($otherFieldLine, ":")) {
                continue; // Invalid line
            }
            $key = substr($otherFieldLine, 0, strpos($otherFieldLine, ":"));
            $value = substr($otherFieldLine, strpos($otherFieldLine, ":") + 1);
            if (isset($oExtraSurveyQuestionCode[$key]) && $value) {
                $relatedColumn = $oExtraSurveyQuestionCode[$key];
                $aOtherFields[$key] = $value;
            } else {
                $this->aQidsWarnings[$qid][] = sprintf(Utilities::translate("Invalid question code %s in survey %s"), $key, $extraSurveyId);
            }
        }
        return $aOtherFields;
    }

    /**
     * Get warnings
     * $var integer $qid, if set force find it, else return current warnings array
     * return array
     */
    public function getQidsWarnings($qid)
    {
        if (!isset($this->aQidsWarnings[$qid])) {
            throw new CException(500, sprintf("Must call getSurveyRestriction bfore warnings for %s", $qid));
        }
        return $this->aQidsWarnings[$qid];
    }
}
