<?php

/**
 * get settings
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2023 Denis Chenu <http://www.sondages.pro>
 * @license AGPL v3
 * @version 0.12.0
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

namespace RelatedSurveyManagement;

use App;

class Settings
{
    /* @var array DefaultSettings : the default settings if not set by DB @see reloadAnyResponse::settings */
    const DefaultSettings = array(
        'deleteAction' => '',
        'deleteActionColumn' => '',
        'deleteActionValue' => '1',
        'checkParentPermission' => '1'
    );
    /* @var string[] settings with global value */
    const Globalettings = [];
    /* @var integer current setting version for PHP */
    const SettingsVersion = 1;

    /**
     * Singleton
     * @var self
     */
    private static $instance = null;

    /**
     * @var null|integre
     */
    private $pluginid;

    /**
     * @var []
     */
    private $settings = array();

    /**
     * @var []
     */
    private $parentIdRelations = array();

    /**
     * constructor
     * @param integer survey id
     */
    public function __construct()
    {
        $oPlugin = \Plugin::model()->find(
            "name = :name",
            array(":name" => 'RelatedSurveyManagement')
        );
        $this->pluginid = $oPlugin->id;
    }

    /**
     * Get the singleton
     * return self
     */
    public static function getInstance()
    {
        if ((null === self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * Get a DB setting from a plugin
     * @param integer survey id
     * @param string setting name
     * @return mixed
     */
    public function getSetting($surveyId, $setting)
    {
        if (isset($this->settings[$surveyId][$setting])) {
            return $this->settings[$surveyId][$setting];
        }

        /* This survey setting */
        $oSetting = \PluginSetting::model()->find(
            'plugin_id = :pluginid AND ' . App()->getDb()->quoteColumnName('key') . ' = :key AND model = :model AND model_id = :surveyid',
            array(
                ':pluginid' => $this->pluginid,
                ':key' => $setting,
                ':model' => 'Survey',
                ':surveyid' => $surveyId,
            )
        );
        if (!empty($oSetting)) {
            $value = json_decode($oSetting->value, true);
            if ($value !== '') {
                $this->settings[$surveyId][$setting] = $value;
                return $value;
            }
        }
        /* empty (string) : then get by default setting */
        if (in_array($setting, self::Globalettings)) {
            $oSetting = \PluginSetting::model()->find(
                'plugin_id = :pluginid AND ' . App()->getDb()->quoteColumnName('key') . ' = :key AND model IS NULL',
                array(
                    ':pluginid' => $this->pluginid,
                    ':key' => $setting,
                )
            );

            if (!empty($oSetting)) {
                $value = json_decode($oSetting->value, true);
                if ($value !== '') {
                    $this->settings[$surveyId][$setting] = $value;
                    return $value;
                }
            }
        }

        /* empty (string) : then get setting by default */
        if (isset(self::DefaultSettings[$setting])) {
            $this->settings[$surveyId][$setting] = self::DefaultSettings[$setting];
            return self::DefaultSettings[$setting];
        }

        /* Then finally : null */
        return null;
    }

    /**
     * Return the valid parent if exist
     * @parem integer $surveyId
     * @return false|integer
     */
    public function getParentId($sid)
    {
        if (isset($this->parentIdRelations[$sid])) {
            if (is_array($this->parentIdRelations[$sid])) {
                return array_key_first($this->parentIdRelations[$sid]);
            }
            return false;
        }
        $this->parentIdRelations[$sid] = false;
        $mainParentId = $this->getSetting($sid, 'mainParent');
        if (empty($mainParentId)) {
            return false;
        }
        $thisMainColumn = $this->getSetting($sid, 'thisMainColumn');
        $parentMainColumn = $this->getSetting($sid, 'parentMainColumn');
        if ($thisMainColumn && $parentMainColumn) {
            $parentMainColumn = \getQuestionInformation\helpers\surveyCodeHelper::getColumnName($mainParentId, $parentMainColumn);
            $thisMainColumn = \getQuestionInformation\helpers\surveyCodeHelper::getColumnName($sid, $thisMainColumn);
            if ($thisMainColumn && $parentMainColumn) {
                $this->parentIdRelations[$sid][$mainParentId] = [$thisMainColumn => $parentMainColumn];
                return $mainParentId;
            }
        }
    }

    /**
     * Return the column relation with parent if exist
     * @parem integer $surveyId
     * @return false|string[] the relation between the survey and the parent survey
     */
    public function getParentRelation($sid)
    {
        $mainParentId = $this->getParentId($sid);
        if ($mainParentId) {
            return $this->parentIdRelations[$sid][$mainParentId];
        }
        return false;
    }
}
