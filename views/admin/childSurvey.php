<li>
    <?php
    echo CHtml::link(
        '<span class="btn btn-xs btn-default"><i class="fa fa-exchange" aria-hidden=1> </i> ' . $id . '</span> ' .
        viewHelper::flatEllipsizeText($title, 80),
        array(
            "admin/pluginhelper",
            "sa" => "sidebody",
            "plugin" => "RelatedSurveyManagement",
            "method" => "actionSettings",
            "surveyid" => $id
        ),
        array('class' => "btn btn-xs btn-link")
    );
    echo CHtml::hiddenField(
        "exportsid[]",
        $id
    );
    if (count($childs)) {
        echo CHtml::tag("ul", array(), '', false);
        foreach ($childs as $childData) {
            Yii::app()->controller->renderPartial('RelatedSurveyManagement.views.admin.childSurvey', $childData, 0);
        }
        echo CHtml::closeTag("ul");
    }
    ?>
</li>
