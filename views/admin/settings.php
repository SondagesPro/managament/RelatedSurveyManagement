<h3 class="clearfix"><?php echo $lang['Related surveys']; ?>
  <div class='pull-right'>
    <?php
      echo CHtml::link(gT('Close'), $form['close'], array('class' => 'btn btn-default'));
    ?>
  </div>
</h3>

<div>
    <?php foreach ($aSettings as $legend => $settings) {
        $this->widget('ext.SettingsWidget.SettingsWidget', array(
        //'id'=>'summary',
        'title' => $legend,
        //'prefix' => $pluginClass, This break the label (id!=name)
        'form' => false,
        'formHtmlOptions' => array(
            'class' => 'form-core',
        ),
        'labelWidth' => 6,
        'controlWidth' => 6,
        'settings' => $settings,
        ));
    } ?>
</div>
<?php
/* The buttons #2 */
?>
<div class='row'>
  <div class='text-center submit-buttons'>
    <?php
      echo CHtml::link(gT('Close'), $form['close'], array('class' => 'btn btn-default'));
    ?>
  </div>
</div>

</form>

<!-- Modal for update questions -->
<div class="modal fade" id="set-new-survey-modal" tabindex="-1" role="dialog" aria-labelledby="set-new-survey-modal-title">
  <?php echo CHtml::form($form['updateSurvey']); ?>
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="<?=$lang['Close']?>"><span aria-hidden="true">&times;</span></button>
        <div class="h4 modal-title" id="set-new-survey-title"><?=$lang['Update related survey']?></div>
      </div>
      <?php if ($updatepermission) : ?>
      <div class="modal-body form-inline">
            <label for="selectsurvey" class="control-label"><?=$lang['Choose related survey']?></label>
            <select id='selectsurvey' name='selectsurvey' class="form-control" required>
                <?php echo getSurveyList(false); ?>
            </select>
            <input type="hidden" id="selectsurvey-question" name="selectsurvey-question" value="0">
      </div>
      <?php endif; ?>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?=$lang['Close']?></button>
        <?php
            echo CHtml::htmlButton('<i class="fa fa-check" aria-hidden="true"></i> ' . gT('Save'), array('type' => 'submit','name' => 'save' . $pluginClass,'value' => 'save','class' => 'btn btn-primary btn-save'));
        ?>
      </div>
    </div>
  </div>
  </form>
</div>
<script>
  RelatedSurveyManagement.init();
</script>
