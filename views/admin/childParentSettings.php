<div class="panel panel-default">
  <div class="panel-heading">
    <div class="panel-title"><?= sprintf(
        $lang['With parent survey %s %s'],
        '<em>' . viewHelper::flatEllipsizeText($title, 80) . '</em>',
        CHtml::link($id, $url, ["class" => 'btn btn-default btn-xs'])
    ); ?></div>
  </div>
  <div class="panel-body">
    <div class="form-group">
      <label class="default control-label col-sm-6" for="childsLink_<?= $id ?>_current"><?= $lang['Column from this survey'] ?></label>
      <div class="col-sm-6">
      <?php
        App()->getController()->widget(
            'yiiwheels.widgets.select2.WhSelect2',
            [
              'data' => $aQuestionList['data'],
              'name' => "childsLink[{$id}][current]",
              'value' => $currents['current'],
              'pluginOptions' => [
                  'minimumResultsForSearch' => 8,
                  'dropdownAutoWidth' => true,
                  'width' => "js: function(){ return Math.max.apply(null, $(this.element).find('option').map(function() { return $(this).text().length; }))+'em' }",
              ],
              'htmlOptions' => array(
                  'empty' => $lang['None'],
                  'options' => $aQuestionList['options'],
                  'class' => 'form-control'
              ),
            ]
        );
        ?>
      </div>
    </div>
    <div class="form-group">
      <label class="default control-label col-sm-6" for="childsLink_<?= $id ?>_child"><?= $lang['Column from child survey'] ?></label>
      <div class="col-sm-6">
      <?php
        App()->getController()->widget(
            'yiiwheels.widgets.select2.WhSelect2',
            [
              'data' => $aChildQuestionList['data'],
              'name' => "childsLink[{$id}][child]",
              'value' => $currents['child'],
              'pluginOptions' => [
                  'minimumResultsForSearch' => 8,
                  'dropdownAutoWidth' => true,
                  'width' => "js: function(){ return Math.max.apply(null, $(this.element).find('option').map(function() { return $(this).text().length; }))+'em' }",
              ],
              'htmlOptions' => array(
                  'empty' => $lang['None'],
                  'options' => $aChildQuestionList['options'],
                  'class' => 'form-control'
              ),
            ]
        );
        ?>
      </div>
    </div>
    <div class="form-group">
      <label class="default control-label col-sm-6" for="childsLink_<?= $id ?>_deleteWith"><?= $lang['Delete child when parent deleted'] ?></label>
      <div class="col-sm-6">
      <?php
        echo CHtml::tag(
            'div',
            [],
            $this->widget('yiiwheels.widgets.switch.WhSwitch', array(
                'name' => "childsLink[{$id}][deleteWith]",
                'value' => $currents['deleteWith'],
                'onLabel' => gT('On'),
                'offLabel' => gT('Off'),
                'htmlOptions' => [],
            ), true)
        );
        ?>
        <div class="help-block"><?= $lang['Delete using delete action of the child.'] ?></div>
        <?php if (!$getQuestionInformationCompat) : ?>
            <div class="has-error"><strong class="help-block"><?= $lang['You need getQuestionInformation version 3.1.0 or up to delete childs.'] ?> </strong></div>
        <?php endif; ?>
      </div>
    </div>
  </div>
</div>
