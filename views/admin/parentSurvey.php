<div class="panel-heading">
    <?php echo CHtml::link(
        ' ',
        array(
            "admin/pluginhelper",
            "sa" => "sidebody",
            "plugin" => "RelatedSurveyManagement",
            "method" => "actionSettings",
            "surveyid" => $id
        ),
        array(
            'class' => "",
            'aria-hidden' => true,
        )
    ); ?>
    <?php echo CHtml::link(
        '<span class="btn btn-xs btn-default"><i class="fa fa-exchange" aria-hidden=1> </i> ' . $id . '</span> ' .
        viewHelper::flatEllipsizeText($title, 80),
        array(
            "admin/pluginhelper",
            "sa" => "sidebody",
            "plugin" => "RelatedSurveyManagement",
            "method" => "actionSettings",
            "surveyid" => $id
        ),
        array('class' => "btn btn-xs btn-link")
    ); ?>
</div>
<div class="panel-body">
    <?php
    if (intval(App()->getConfig('versionnumber')) > 3) {
        echo CHtml::link(
            $aQuestion['title'],
            array(
                "questionAdministration/view",
                "qid" => $aQuestion['qid'],
                "gid" => $aQuestion['gid'],
                "surveyid" => $aQuestion['sid']
            ),
            array('class' => "btn btn-xs btn-default")
        );
    } else {
        echo CHtml::link($aQuestion['title'], array("admin/questions/sa/view","qid" => $aQuestion['qid']), array('class' => "btn btn-xs btn-default"));
    }
    ?>
    <?php echo viewHelper::flatEllipsizeText($aQuestion['question'], 60); ?>
    <div class="">
        <ul class="">
            <?php if ($restriction['id']) {
                echo "<li>" . sprintf($lang['Response id relation in %s'], $restriction['id']) . "</li>";
            } ?>
            <li><?php if (!$restriction['token']) {
                 echo "<strong>" . $lang['Token not used'] . "</strong>";
                } elseif ($restriction['token'] == 'group') {
                    echo $lang['Token used with token group'];
                } else {
                    echo $lang['Token used'];
                } ?></li>
            <?php if (!empty($restriction['other'])) { ?>
                <li><strong><?= $lang['Other relations used'] ?></strong><ul>
                   <?php  foreach ($restriction['other'] as $qCode => $value) {
                        echo "<li>" . sprintf("%s related with %s", "<code>" . $qCode . "</code>", "<code>" . $value . "</code>") . "</li>";
                   } ?>
                </ul></li>
            <?php } ?>
        </ul>
    </div>
    <?php if ($warnings) { ?>
        <ul class="alert alert-warning">
            <?php foreach ($warnings as $warning) { ?>
                <li><?= $warning ?></li>
            <?php } ?>  
        </ul>
    <?php } ?>
</div>
