<div class="panel-heading">
    <?php
    if (intval(App()->getConfig('versionnumber')) > 3) {
        echo CHtml::link(
            $aQuestion['title'],
            array(
                "questionAdministration/view",
                "qid" => $aQuestion['qid'],
                "gid" => $aQuestion['gid'],
                "surveyid" => $aQuestion['sid']
            ),
            array('class' => "btn btn-xs btn-default")
        );
    } else {
        echo CHtml::link($aQuestion['title'], array("admin/questions/sa/view","qid" => $aQuestion['qid']), array('class' => "btn btn-xs btn-default"));
    }
    ?>
    <?php echo viewHelper::flatEllipsizeText($aQuestion['question'], 60); ?>
</div>
<div class="panel-body">
    <div class="clearfix">
        <div class="pull-right">
            <button class='btn btn-default btn-sm set-new-survey-action' type='button' data-question-id='<?= $qid ?>'><?= $lang['Change survey'] ?></button>
        </div>
        <div class="h4">
            <ul class="text-danger list-unstyled">
                <?php foreach ($errors as $error) { ?>
                    <li><?= $error ?></li>
                <?php } ?>  
            </ul>
        </div>
    </div>
</div>
