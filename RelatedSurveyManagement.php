<?php

/**
 * relatedSurveyManagement management of related survey
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2020-2024 Denis Chenu <www.sondages.pro>
 * @copyright 2020-2021 OECD <www.oecd.pro>
 * @license AGPL v3
 * @version 0.12.1
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
class RelatedSurveyManagement extends PluginBase
{
    protected static $name = 'RelatedSurveyManagement';
    protected static $description = 'Management of related survey';

    protected $storage = 'DbStorage';

    /** @inheritdoc */
    public $allowedPublicMethods = array(
        'actionSettings',
        'actionSaveSettings',
        'actionUpdateSurvey',
        'actionChildSettings',
        'actionSaveChildSettings',
    );

    /** @inheritdoc */
    public function init()
    {
        Yii::setPathOfAlias(get_class($this), dirname(__FILE__));
        App()->setConfig('RelatedSurveyManagementApiVersion', \RelatedSurveyManagement\Utilities::API);
        /* Add settings update */
        $this->subscribe('beforeToolsMenuRender');
        /* Settings for ReloadAnyEditDisabled */
        $this->subscribe('ReloadAnyEditDisabled');
        /** Add {PARENTURL} and @@PARENTURL@@ for replacement **/
        if (intval(App()->getConfig('versionnumber')) > 3) {
            $this->subscribe('beforeTokenEmail');
        }
    }

    /**
     * see beforeTokenEmail event
     * Check if need to replace PARENTURL
     */
    public function beforeTokenEmail()
    {
        $mailevent = $this->getEvent();
        $mailer = $mailevent->get('mailer');
        $rawBody = $mailer->rawBody;
        if (!strpos($rawBody, "{PARENTURL}") && !strpos($rawBody, "@@PARENTURL@@")) {
            return;
        }
        if (array_key_exists('PARENTURL', $mailer->aReplacements)) {
            // Already done by another plugin
            return;
        }
        if (empty($mailer->rawSubject) || empty($mailer->rawBody)) {
            // Not from core, can not do it …
            return;
        }
        $surveyid = $mailevent->get('survey');
        $token = $mailevent->get('token');
        $language = $mailer->mailLanguage;
        $oParentSurvey = new \RelatedSurveyManagement\ParentSurveys($surveyid);
        $parentSurvey = $oParentSurvey->getFinalAncestrySurvey();
        if ($parentSurvey) {
            $surveyid = $parentSurvey;
        }
        $mailer->aReplacements['PARENTURL'] = App()->getController()
        ->createAbsoluteUrl(
            "/survey/index",
            ["surveyid" => $surveyid, "token" => $token,"langcode" => $language]
        );
        $subject = $mailer->doReplacements($mailer->rawSubject);
        $body = $this->doReplacements($mailer->rawBody);

        if ($mailer->CharSet != $mailer->BodySubjectCharset) {
            /* Must test this … */
            $subject = mb_convert_encoding($subject, $mailer->CharSet, $mailer->BodySubjectCharset);
            $body = mb_convert_encoding($body, $mailer->CharSet, $mailer->BodySubjectCharset);
        }
        $mailer->Subject = $subject;
        $mailer->Body = $body;
        $mailevent->set('subject', $subject);
        $mailevent->set('body', $body);
        /* All done */
    }

    /**
     * see beforeToolsMenuRender event
     * @deprecated ? See https://bugs.limesurvey.org/view.php?id=15476
     * @return void
     */
    public function beforeToolsMenuRender()
    {
        $event = $this->getEvent();
        $surveyId = $event->get('surveyId');
        if (!Permission::model()->hasSurveyPermission($surveyId, 'surveysettings', 'update')) {
            return;
        }
        if (!Permission::model()->hasSurveyPermission($surveyId, 'surveycontent', 'read')) {
            return;
        }
        $aMenuItem = array(
            'label' => $this->translate('Related Survey Management'),
            'iconClass' => 'fa fa-exchange',
            'href' => Yii::app()->createUrl(
                'admin/pluginhelper',
                array(
                    'sa' => 'sidebody',
                    'plugin' => get_class($this),
                    'method' => 'actionSettings',
                    'surveyId' => $surveyId
                )
            ),
        );
        $menuItem = new \LimeSurvey\Menu\MenuItem($aMenuItem);
        $event->append('menuItems', array($menuItem));
    }

    /**
     * The settings function : show parents and childs
     * @param int $surveyId Survey id
     * @return string
     */
    public function actionSettings($surveyId)
    {
        $oSurvey = Survey::model()->findByPk($surveyId);
        $this->checkAccessSurveySettings($oSurvey);
        /* Get the current survey language */
        $oSurvey =  Survey::model()->findByPk($surveyId);
        $language =  $oSurvey->language;
        /* Basic data */
        $aData = array(
            'pluginClass' => get_class($this),
            'surveyId' => $surveyId,
        );
        $aData['lang'] = array(
            'Close' => $this->translate("Close"),
            'Current survey : %s' => $this->translate("Current survey : %s"),
            'Related surveys' => $this->translate("Related surveys"),
            'Surveys tree' => $this->translate("Surveys tree"),
            'Update related survey' => $this->translate("Update related survey"),
            'Choose related survey' => $this->translate("Choose related survey"),
            'Response id relation in %s' => $this->translate("Response id relation in %s"),
            'Response id not used' => $this->translate("Response id not used"),
            'Token not used' => $this->translate("Token not used"),
            'Token used' => $this->translate("Token used"),
            'Token used with token group' => $this->translate("Token used with token group"),
            'Other relations used' => $this->translate("Other relations used"),
            '%s filled by %s' => $this->translate("%s related with %s"),
            'Change survey' => $this->translate("Change survey"),
            'Choose survey' => $this->translate("Choose survey"),
        );
        Yii::import('application.helpers.viewHelper');
        $aSettings = array();

        /* parent survey */
        $oParentSurveys = new \RelatedSurveyManagement\ParentSurveys($surveyId);
        $aParentSurveys = $oParentSurveys->getParentSurveys();
        $aSurveys = array();
        foreach ($aParentSurveys as $qid => $survey) {
            $oParentSurvey = Survey::model()->findByPk($survey);
            $aSurveyData = array(
                'id' => $oParentSurvey->sid,
                'title' => $oParentSurvey->getLocalizedTitle(),
                'oParentSurvey' => $oParentSurvey,
                'lang' => $aData['lang'],
            );
            $parentLanguage = $oParentSurvey->language;

            $oQuestion = Question::model()->find("qid = :qid", array(":qid" => $qid));
            if (empty($oQuestion)) {
                $this->log("Question $qid not found for survey $surveyId", 'warning');
                continue;
            }
            $aSurveyData['aQuestion'] = $this->getQuestionInfoForView($qid, $parentLanguage);
            $aSurveyData['restriction'] = $oParentSurveys->getSurveyRestriction($oQuestion->qid);
            $aSurveyData['warnings'] = $oParentSurveys->getQidsWarnings($oQuestion->qid);
            $element = $this->renderPartial('admin.parentSurvey', $aSurveyData, true);
            $aSurveys['question_' . $qid] = array(
                'type' => 'info',
                'content' => $element,
                'class' => array("panel panel-default")
            );
        }
        if (!empty($aSurveys)) {
            $aSettings[$this->translate("Settings as child survey")] = array(
                'linkToChildsSettings' => [
                    'type' => 'info',
                    'content' => Chtml::link(
                        $this->translate("Update settings as child"),
                        [
                            'admin/pluginhelper/sa/sidebody',
                            'plugin' => get_class($this),
                            'method' => 'actionChildSettings',
                            'surveyId' => $surveyId
                        ],
                        ['class' => 'btn btn-block btn-default btn-lg']
                    ),
                    'class' => array("")
                ]
            );
            $aSettings[$this->translate("Parent surveys with question")] = $aSurveys;
        }
        /* Child surveys related to this one */
        /* The related survey with this survey */
        $oChildrenSurveys = new \RelatedSurveyManagement\ChildrenSurveys($surveyId);
        $aChildrensSurveys = $oChildrenSurveys->getChildrensSurveys();
        $getQidsPlugin = $oChildrenSurveys->getQidsPlugin();

        $aQuestions = array();
        foreach ($getQidsPlugin as $qid => $plugin) {
            $aQuestionData = array(
                'qid' => $qid,
                'lang' => $aData['lang'],
            );
            $oQuestion = Question::model()->find("qid = :qid", array(":qid" => $qid));
            $aQuestionData['aQuestion'] = $this->getQuestionInfoForView($qid, $language);
            $aQuestionData['warnings'] = [];
            $aQuestionData['relatedSurvey'] = null;
            if (isset($aChildrensSurveys[$qid])) {
                $aQuestionData['relatedSurvey'] = Survey::model()->findByPk($aChildrensSurveys[$qid]);
                $aQuestionData['restriction'] = $oChildrenSurveys->getChildrenSurveyRestriction($qid);
                $aQuestionData['errors'] = $oChildrenSurveys->getQidsErrors($qid);
                $aQuestionData['warnings'] = $oChildrenSurveys->getQidsWarnings($qid);
                $element = $this->renderPartial('admin.questionWithRelated', $aQuestionData, true);
            } else {
                $aQuestionData['errors'] = $oChildrenSurveys->getQidsErrors($qid);
                $element = $this->renderPartial('admin.questionErrorRelated', $aQuestionData, true);
            }
            $aQuestions['question_' . $qid] = array(
                'type' => 'info',
                'content' => $element,
                'class' => array("panel panel-default")
            );
        }
        if (!empty($aQuestions)) {
            $aSettings[$this->translate("Children surveys by question")] = $aQuestions;
        }
        $aSurveyTree = array();
        foreach ($aChildrensSurveys as $childSurveyId) {
            $oSurvey = Survey::model()->findByPk($childSurveyId);
            if ($oSurvey) {
                $aSurveyTree[$childSurveyId] = array(
                    'id' => $childSurveyId,
                    'title' => $oSurvey->getLocalizedTitle(),
                    'childs' => $this->getChildsOfSurvey($childSurveyId),
                );
            } else {
                // Done before ?
            }
        }
        $content = "";
        if (!empty($aSurveyTree)) {
            $content = $this->renderPartial(
                'admin.treeSurveys',
                array_merge($aData, array('aSurveyTree' => $aSurveyTree)),
                true
            );
            $aTree = array(
                'tree' => array(
                    'type' => 'info',
                    'content' => $content,
                    'class' => array("well")
                )
            );
            $aSettings[$this->translate("Tree of childs surveys")] = $aTree;
        }
        $aData['aSettings'] = $aSettings;
        $aData['form'] = array(
            'action' => App()->createUrl(
                'admin/pluginhelper/sa/sidebody',
                ['plugin' => get_class($this),'method' => 'actionSaveSettings','surveyId' => $surveyId]
            ),
            'updateSurvey' => App()->createUrl(
                'admin/pluginhelper/sa/sidebody',
                ['plugin' => get_class($this),'method' => 'actionUpdateSurvey','surveyId' => $surveyId]
            ),
            'close' => App()->createUrl(
                'surveyAdministration/view',
                ['surveyid' => $surveyId]
            ),
        );

        if (intval(App()->getConfig('versionnumber')) <= 3) {
            $aData['form']['close'] = Yii::app()->createUrl('admin/survey/sa/view', array('surveyId' => $surveyId));
        }
        $aData['updatepermission'] = Permission::model()->hasSurveyPermission($surveyId, 'surveysettings', 'update');
        $content = $this->renderPartial('admin.settings', $aData, true);

        Yii::app()->clientScript->addPackage('RelatedSurveyManagement', array(
            'basePath'    => get_class($this) . '.assets',
            'js'          => array('RelatedSurveyManagement.js'),
            'css'          => array('RelatedSurveyManagement.css'),
            'depends'      => array('jquery'),
        ));
        Yii::app()->getClientScript()->registerPackage('RelatedSurveyManagement');
        return $content;
    }

    /**
     * The settings function when have parents
     * @param int $surveyId Survey id
     * @return string
     */
    public function actionChildSettings($surveyId)
    {
        $oSurvey = Survey::model()->findByPk($surveyId);
        $this->checkAccessSurveySettings($oSurvey);
        /* Get the current survey language */
        $oSurvey =  Survey::model()->findByPk($surveyId);
        $surveyColumnsInformation = new \getQuestionInformation\helpers\surveyColumnsInformation($surveyId, App()->getLanguage());
        $surveyColumnsInformation->ByEmCode = true;
        $aQuestionList = $surveyColumnsInformation->allQuestionListData();
        /* Basic data */
        $aData = array(
            'pluginClass' => get_class($this),
            'surveyId' => $surveyId,
        );
        /* Lang */
        $aData['lang'] = array(
            'Close' => $this->translate("Close"),
            'Survey settings as child' => $this->translate("Survey settings as child"),
            'With parent survey %s %s' => $this->translate("With parent survey %s %s"),
            'Response relation with parent' => $this->translate("Response relation with parent"),
            'Link is done with equality between value of the 2 columns' => $this->translate("Link is done with equality between value of the 2 columns"),
            'Column from parent survey' => $this->translate("Column from parent survey"),
            'Column from this survey' => $this->translate("Column from this survey"),
            'Column from child survey' => $this->translate("Column from child survey"),
            'Delete child when parent deleted' => $this->translate("Delete child when parent deleted"),
            'Delete using delete action of the child.' => $this->translate("Delete using delete action of the child."),
            'You need getQuestionInformation version 3.1.0 or up to delete childs.' => $this->translate("You need getQuestionInformation version 3.1.0 or up to delete childs."),
            'None' => $this->translate('None'),
        );
        /* Allow to update even if there are no parent : why not ? */
        $aSettings[$this->translate('Action to do when deleted from parent or by plugins')] = [
            'deleteAction' => array(
                'type' => 'select',
                'label' => $this->translate("Action to do when deleted by parent survey."),
                'options' => array(
                    'column' => $this->translate("Update a column"),
                ),
                'htmlOptions' => array(
                    'empty' => $this->translate("Delete response"),
                ),
                'current' => $this->get('deleteAction', 'Survey', $surveyId, 0),
                'help' => $this->translate('Delete action need plugin using it. If plugin are not compatible : response can be deleted by plugin.'),
            ),
            'deleteActionColumn' => array(
                'type' => 'select',
                'label' => $this->translate('Survey columns to be updated'),
                'options' => $aQuestionList['data'],
                'htmlOptions' => array(
                    'empty' => $this->translate("Delete the response"),
                    'options' => $aQuestionList['options'], // In dropdown, but not in select2
                ),
                'current' => $this->get('deleteActionColumn', 'Survey', $surveyId)
            ),
            'deleteActionValue' => array(
                'type' => 'string',
                'label' => $this->translate('Value to be set'),
                'current' => $this->get('deleteActionValue', 'Survey', $surveyId, '')
            ),
        ];

        /* primary parent */
        $oParentSurveys = new \RelatedSurveyManagement\ParentSurveys($surveyId);
        $aParentSurveys = array_unique($oParentSurveys->getParentSurveys());
        $criteria = new CDBCriteria();
        $criteria->select = 't.sid';
        $criteria->with = ["defaultlanguage" => ["select" => "surveyls_title"]];
        $criteria->order = 'surveyls_title';

        if (!$this->get('allowAnyParentAsMain', 'Survey', $surveyId)) {
            $criteria->addInCondition('sid', $aParentSurveys);
        }
        $aWholeSurveys = Survey::model()
            ->permission(Yii::app()->user->getId())
            ->with('defaultlanguage')
            ->findAll($criteria);
        $aWholeSurveysOption = CHtml::listData($aWholeSurveys, 'sid', function ($oSurvey) {
            if (empty($oSurvey->defaultlanguage)) {
                return $oSurvey->sid;
            }
            return CHtml::encode($oSurvey->defaultlanguage->surveyls_title) . " [" . $oSurvey->sid . "]";
        });
        $mainParent = $this->get('mainParent', 'Survey', $surveyId, null);
        if ($this->get('allowAnyParentAsMain', 'Survey', $surveyId)) {
            $listSurveysData = \RelatedSurveyManagement\Utilities::getSurveysListData([], $mainParent);
        } else {
            if (!empty($mainParent) && Survey::model()->findByPk($mainParent)) {
                array_unshift($aParentSurveys, $mainParent);
                $aParentSurveys = array_unique($aParentSurveys);
            }
            $listSurveysData = \RelatedSurveyManagement\Utilities::getSurveysListData($aParentSurveys, $mainParent);
        }
        $aSettings[$this->translate('Main parent')] = [
            'allowAnyParentAsMain' => array(
                'type' => 'boolean',
                'label' => $this->translate("Allow any parent for main parent."),
                'current' => $this->get('allowAnyParentAsMain', 'Survey', $surveyId, 0),
                'help' => $this->translate('This allow you to choose any survey as main parent for plugin use main parent. Remind to select relation with main parent.'),
            ),
            'mainParent' => array(
                'type' => 'select',
                'htmlOptions' => array(
                    'empty' => $this->translate("None"),
                ),
                'label' => $this->translate("Main parent survey."),
                'options' => $listSurveysData,
                'current' => $mainParent,
                'help' => $this->translate("used by specific plugin. List depend of the previous setting, if you want to use any surey, you need to allow it before."),
            ),
        ];
        /* Allow link with current */
        if ($mainParent) {
            $aSettings[$this->translate('Main parent')]['thisMainColumn'] = array(
                'type' => 'select',
                'label' => $this->translate('Column from this survey'),
                'options' => $aQuestionList['data'],
                'htmlOptions' => array(
                    'empty' => $this->translate("None"),
                    'options' => $aQuestionList['options'], // In dropdown, but not in select2
                ),
                'current' => $this->get('thisMainColumn', 'Survey', $surveyId)
            );
            $surveyColumnsInformation = new \getQuestionInformation\helpers\surveyColumnsInformation($surveyId, App()->getLanguage());
            $surveyColumnsInformation->ByEmCode = true;
            $aParentQuestionList = $surveyColumnsInformation->allQuestionListData();
            $aParentQuestionList = [
                'data' => ['id' => $this->translate("Response id")] + $aParentQuestionList['data'],
                'options' => ['id' => [
                    'data-html' => true,
                    'data-trigger' => 'hover focus',
                    'data-content' => $this->translate("Response id"),
                    'data-title' => $this->translate("Response id"),
                    'title' => $this->translate("Response id"),
                    ]
                ] + $aParentQuestionList['options']
            ];
            $aSettings[$this->translate('Main parent')]['parentMainColumn'] = array(
                'type' => 'select',
                'label' => $this->translate('Related with column from parent survey'),
                'options' => $aParentQuestionList['data'],
                'htmlOptions' => array(
                    'empty' => $this->translate("None"),
                    'options' => $aParentQuestionList['options'], // In dropdown, but not in select2
                ),
                'current' => $this->get('parentMainColumn', 'Survey', $surveyId)
            );
            /* reloadAnyResponse restriction */
            if (App()->getConfig('reloadAnyResponseApi')) {
                $aSettings[$this->translate('Main parent')]['checkParentPermission'] = array(
                    'type' => 'boolean',
                    'label' => $this->translate("Check parent permission before check this survey permission when try to reload response."),
                    'current' => $this->get('checkParentPermission', 'Survey', $surveyId, 1),
                    'help' => $this->translate('If this survey allow reload, permission to reload parent is checked before checking this permission.')
                );
            }
        }

        /* Childs direct relation */
        $aChildsCurrentSettings = (array) $this->get('childsLink', 'Survey', $surveyId, []);
        $childsSurveyId = array_unique(\RelatedSurveyManagement\ChildrenSurveys::getChildsSurveys($surveyId));
        /* Add id at start to aQuestionList */
        $aThisASParentQuestionList = [
            'data' => ['id' => $this->translate("Response id")] + $aQuestionList['data'],
            'options' => ['id' => [
                'data-html' => true,
                'data-trigger' => 'hover focus',
                'data-content' => $this->translate("Response id"),
                'data-title' => $this->translate("Response id"),
                'title' => $this->translate("Response id"),
                ]
            ] + $aQuestionList['options']
        ];
        foreach ($childsSurveyId as $childSurveyId) {
            $oChildSurvey = Survey::model()->findByPk($childSurveyId);
            $childSurveyColumnsInformation = new \getQuestionInformation\helpers\surveyColumnsInformation($childSurveyId, App()->getLanguage());
            $childSurveyColumnsInformation->ByEmCode = true;
            $aChildQuestionList = $childSurveyColumnsInformation->allQuestionListData();
            $currents = ['current' => null, 'child' => null, 'deleteWith' => null];
            if (isset($aChildsCurrentSettings[$childSurveyId])) {
                $currents = array_merge($currents, $aChildsCurrentSettings[$childSurveyId]);
            }
            $aChildRelationsData = array(
                'id' => $oChildSurvey->sid,
                'title' => $oChildSurvey->getLocalizedTitle(),
                'url' => App()->createUrl('admin/pluginhelper/sa/sidebody', array('plugin' => get_class($this), 'method' => 'actionSettings', 'surveyId' => $childSurveyId)),
                'oChildSurvey' => $oChildSurvey,
                'lang' => $aData['lang'],
                'aQuestionList' => $aThisASParentQuestionList,
                'aChildQuestionList' => $aChildQuestionList,
                'currents' => $currents,
                'getQuestionInformationCompat' => version_compare(App()->getConfig('getQuestionInformationAPI'), "3.1.0", ">=")
            );
            $element = $this->renderPartial('admin.childParentSettings', $aChildRelationsData, true);
            $aSettingsChildsRelation['relation_' . $childSurveyId] = [
                'type' => 'info',
                'content' => $element
            ];
        }
        if (!empty($aSettingsChildsRelation)) {
            $aSettings[$this->translate('Direct relation with childs (SQL join)')] = $aSettingsChildsRelation;
        }

        /* Add parent relation system : needed for find parent plugin and delete when parent */
        $aData['aSettings'] = $aSettings;
        $aData['updatepermission'] = Permission::model()->hasSurveyPermission($surveyId, 'surveysettings', 'update');
        $aData['form'] = array(
            'action' => App()->createUrl('admin/pluginhelper/sa/sidebody', array('plugin' => get_class($this),'method' => 'actionSaveChildSettings','surveyId' => $surveyId)),
            'close' => App()->createUrl('admin/pluginhelper/sa/sidebody', array('plugin' => get_class($this),'method' => 'actionSettings','surveyId' => $surveyId))
        );
        return $this->renderPartial('admin.childsettings', $aData, true);
    }

    /**
     * The save action
     * @param int $surveyId Survey id
     * @return void (redirect)
     */
    public function actionSaveChildSettings($surveyId)
    {
        if (empty(App()->getRequest()->getPost('save' . get_class($this)))) {
            throw new CHttpException(400);
        }
        $oSurvey = Survey::model()->findByPk($surveyId);
        $this->checkAccessSurveySettings($oSurvey, 'update');
        $inputs = array(
            'deleteAction',
            'deleteActionColumn',
            'deleteActionValue',
            'parentDeletedAction',
            'allowAnyParentAsMain',
            'mainParent',
            'thisMainColumn',
            'parentMainColumn',
            'checkParentPermission',
            'childsLink',
        );
        foreach ($inputs as $input) {
            $this->set(
                $input,
                App()->getRequest()->getPost($input),
                'Survey',
                $surveyId
            );
        }
        if (App()->getRequest()->getPost('save' . get_class($this)) == 'redirect') {
            $redirectUrl = Yii::app()->createUrl('admin/pluginhelper/sa/sidebody', array('plugin' => get_class($this),'method' => 'actionSettings','surveyId' => $surveyId));
            Yii::app()->getRequest()->redirect($redirectUrl);
        }
        $redirectUrl = Yii::app()->createUrl('admin/pluginhelper/sa/sidebody', array('plugin' => get_class($this),'method' => 'actionChildSettings','surveyId' => $surveyId));
        Yii::app()->getRequest()->redirect($redirectUrl, true, 303);
    }

    /**
     * The save action
     * @param int $surveyId Survey id
     * @return void (redirect)
     */
    public function actionSaveSettings($surveyId)
    {
        if (empty(App()->getRequest()->getPost('save' . get_class($this)))) {
            throw new CHttpException(400);
        }
        $oSurvey = Survey::model()->findByPk($surveyId);
        $this->checkAccessSurveySettings($oSurvey, 'update');

        if (App()->getRequest()->getPost('save' . get_class($this)) == 'redirect') {
            $redirectUrl = Yii::app()->createUrl('admin/survey/sa/view', array('surveyId' => $surveyId));
            Yii::app()->getRequest()->redirect($redirectUrl);
        }
        $redirectUrl = Yii::app()->createUrl('admin/pluginhelper/sa/sidebody', array('plugin' => get_class($this),'method' => 'actionSettings','surveyId' => $surveyId));
        Yii::app()->getRequest()->redirect($redirectUrl, true, 303);
    }

    /**
     * The save action when update a related survey id
     * @param int $surveyId Survey id
     * @return void (redirect)
     */
    public function actionUpdateSurvey($surveyId)
    {
        if (empty(App()->getRequest()->getPost('save' . get_class($this)))) {
            throw new CHttpException(400);
        }
        $oSurvey = Survey::model()->findByPk($surveyId);
        $this->checkAccessSurveySettings($oSurvey, 'update');
        $qid = Yii::app()->getRequest()->getParam("selectsurvey-question");
        if (empty($qid)) {
            throw new CHttpException(400);
        }
        $newSid = Yii::app()->getRequest()->getParam("selectsurvey");
        if (empty($newSid)) {
            throw new CHttpException(400);
        }
        $oQuestion = Question::model()->find("qid = :qid", array(":qid" => $qid));
        if (!$oQuestion || $oQuestion->sid != $surveyId) {
            throw new CHttpException(400);
        }
        $availableRelatedPlugins = \RelatedSurveyManagement\Utilities::getAvailableRelatedPlugins();
        $relatedPlugin = Yii::app()->getRequest()->getParam("relatedPlugin");
        if (empty($relatedPlugin)) {
            foreach ($availableRelatedPlugins as $plugin => $aPluginSettings) {
                $oAttribute = QuestionAttribute::model()->find(
                    "qid =:qid and attribute = :attribute and value <> ''",
                    array(":qid" => $qid ,":attribute" => $aPluginSettings['surveyid'])
                );
                if ($oAttribute) {
                    break;
                }
            }
        } else {
            if (!empty($availableRelatedPlugins[$relatedPlugin]['surveyid'])) {
                $oAttribute = QuestionAttribute::model()->find(
                    "qid =:qid and attribute = :attribute and value <> ''",
                    array(":qid" => $qid ,":attribute" => $availableRelatedPlugins[$relatedPlugin]['surveyid'])
                );
                if (!$oAttribute) {
                    $oAttribute = new QuestionAttribute();
                    $oAttribute->qid = $qid;
                    $oAttribute->setAttribute("attribute", $availableRelatedPlugins[$relatedPlugin]['surveyid']);
                }
            }
        }
        if (empty($oAttribute)) {
            throw new CHttpException(400);
        }
        if ($oAttribute->getAttribute("value") != $newSid) {
            /* Check permission on new sid */
            if (Permission::model()->hasSurveyPermission($newSid, 'responses', 'update')) {
                $oAttribute->setAttribute("value", $newSid);
                if ($oAttribute->save()) {
                    App()->setFlashMessage($this->translate("Related survey saved"), 'success');
                } else {
                    App()->setFlashMessage($this->translate("An error happen when save related survey"), 'danger');
                }
            } else {
                App()->setFlashMessage(sprintf($this->translate("You do not have sufficient right on survey %s"), $newSid), 'warning');
            }
        }

        if (App()->getRequest()->getPost('save' . get_class($this)) == 'redirect') {
            $redirectUrl = Yii::app()->createUrl('admin/survey/sa/view', array('surveyId' => $surveyId));
            Yii::app()->getRequest()->redirect($redirectUrl);
        }
        $redirectUrl = Yii::app()->createUrl('admin/pluginhelper/sa/sidebody', array('plugin' => get_class($this),'method' => 'actionSettings','surveyId' => $surveyId));
        Yii::app()->getRequest()->redirect($redirectUrl, true, 303);
    }

    /**
     * Ebent
     */
    public function reloadAnyEditDisabled()
    {
        $reloadAnyEditDisabledEvent = $this->getEvent();
        $RelatedSurveyManagementSettings = \RelatedSurveyManagement\Settings::getInstance();
        $surveyId = $reloadAnyEditDisabledEvent->get('surveyId');
        $srid = $reloadAnyEditDisabledEvent->get('srid');
        $token = $reloadAnyEditDisabledEvent->get('token');
        $parentId = $RelatedSurveyManagementSettings->getParentId($surveyId);
        if (empty($parentId)) {
            return;
        }
        if (!$RelatedSurveyManagementSettings->getSetting($surveyId, 'checkParentPermission')) {
            return;
        }
        $relationWithParent = $RelatedSurveyManagementSettings->getParentRelation($surveyId);
        if (empty($relationWithParent)) {
            return;
        }
        $selectChildColumn = $childColumn = array_key_first($relationWithParent);
        $selectParentColumn = $parentColumn = reset($relationWithParent);
        $selectChildColumn = App()->db->quoteColumnName($selectChildColumn);
        $selectParentColumn = App()->db->quoteColumnName($selectParentColumn);

        $oChildResponse = Response::model($surveyId)->find(array(
            'select' => ['id', $selectChildColumn],
            'condition' => 'id = :id',
            'params' => [':id' => $srid]
        ));
        if (empty($oChildResponse) || empty($oChildResponse->getAttribute($childColumn))) {
            return;
        }
        $oParentResponse = Response::model($parentId)->find(array(
            'select' => ['id', $selectParentColumn],
            'condition' => App()->db->quoteColumnName($parentColumn) . ' = :childValue',
            'params' => [':childValue' => $oChildResponse->getAttribute($childColumn)]
        ));
        if (empty($oParentResponse)) {
            return;
        }
        $oStartUrl =  new \reloadAnyResponse\StartUrl($parentId, $token);
        if ($oStartUrl->getUrl($oParentResponse->id)) {
            return;
        }
        $reloadAnyEditDisabledEvent->set('disable', true);
    }
    /**
     * Get the childs survey in a tree way
     * @param $surveyId
     * @return array[]
     */
    private function getChildsOfSurvey($surveyId)
    {
        return \RelatedSurveyManagement\ChildrenSurveys::getChildsOfSurvey($surveyId);
    }

    /**
     * get aray of question information as object for view
     * @param integer $qid
     * @param string $language
     *
     * @return string[]
     */
    private function getQuestionInfoForView($qid, $language)
    {
        if (intval(App()->getConfig('versionnumber')) <= 3) {
            $oQuestion = Question::model()->find("qid = :qid and language = :language", array(":qid" => $qid, ":language" => $language));
            if (empty($oQuestion)) {
                $oQuestion = Question::model()->find("qid = :qid ", array(":qid" => $qid));
            }
            return $oQuestion->getAttributes();
        }
        $oQuestion = Question::model()->find("qid = :qid", array(":qid" => $qid));
        $oQuestionL10n = QuestionL10n::model()->find("qid = :qid and language = :language", array(":qid" => $qid, ":language" => $language));
        if (empty($oQuestionL10n)) {
            $oQuestionL10n = QuestionL10n::model()->find("qid = :qid ", array(":qid" => $qid));
        }
        return array_merge(
            $oQuestion->getAttributes(),
            $oQuestionL10n->getAttributes()
        );
    }

    /**
     * Check validity of survey for updating settings
     * @param \Survey $survey
     * @param string $action to be tested on surveysettings Permission
     * @throws Exception
     * @return void
     */
    private function checkAccessSurveySettings($survey, $action = 'read')
    {
        if (!$survey) {
            throw new CHttpException(404, gT("This survey does not seem to exist."));
        }
        if (!Permission::model()->hasSurveyPermission($survey->sid, 'surveysettings', $action)) {
            throw new CHttpException(403);
        }
        if (!Permission::model()->hasSurveyPermission($survey->sid, 'surveycontent', 'read')) {
            throw new CHttpException(403);
        }
    }

    /*******************************************************
     * Common for a lot of plugin, helper for compatibility
     *******************************************************/

    /** @inheritdoc
     * @see event afterPluginLoad
     **/

    public function afterPluginLoad()
    {
        $messageSource = array(
            'class' => 'CGettextMessageSource',
            'cacheID' => get_class($this) . 'Lang',
            'cachingDuration' => 0,
            'forceTranslation' => true,
            'useMoFile' => true,
            'basePath' => __DIR__ . DIRECTORY_SEPARATOR . 'locale',
            'catalog' => 'messages',// default from Yii
        );
        Yii::app()->setComponent(get_class($this) . 'Messages', $messageSource);
    }

     /**
      * get translation
      * @param string $string to translate
      * @param string language, current by default
      * @return string
      */
    private function translate($string, $sLanguage = null)
    {
        return \RelatedSurveyManagement\Utilities::translate($string, $sLanguage);
    }

     /**
     * @inheritdoc adding string, by default current event
     * @param string $message
     * @param string $level From CLogger, defaults to CLogger::LEVEL_TRACE
     * @param string $logDetail
     */
    public function log($message, $level = \CLogger::LEVEL_TRACE, $logDetail = null)
    {
        if (!$logDetail && $this->getEvent()) {
            $logDetail = $this->getEvent()->getEventName();
        } // What to put if no event ?
        if ($logDetail) {
            $logDetail = "." . $logDetail;
        }
        $category = get_class($this);
        \Yii::log($message, $level, 'plugin.' . $category . $logDetail);
    }
}
