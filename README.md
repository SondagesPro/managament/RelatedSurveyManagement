# RelatedSurveyManagement

See and manage Related Survey using questionExtraSurvey or questionSpreadsheetSurvey

## Related plugins

- [questionExtraSurvey](https://gitlab.com/SondagesPro/QuestionSettingsType/questionExtraSurvey)
- [questionSpreadsheetSurvey](https://gitlab.com/SondagesPro/QuestionSettingsType/questionSpreadsheetSurvey)

## Quick usage

After activattion : a new tool menu are shown where you can see the related survey (parents and childs).

You can replace existing related child surveys by new one.

### Advanced usage

For childs survey : you can replace deletion action by updating a column and set a specific values. This settings are found on _Settings as child survey_ action button

You can use like this

**Filtering by the deleted response**

```
$oCriteria = new CDbCriteria();
if (version_compare(App()->getConfig('RelatedSurveyManagementApiVersion', "0.0"), "0.9" , ">=")) {
    $RelatedSurveysHelper = \RelatedSurveyManagement\RelatedSurveysHelper::getInstance($surveyId);
    $oCriteria = $RelatedSurveysHelper->addFilterDeletedCriteria($oCriteria);
}
```

**Update columns if set in place of deletion**

```
if (version_compare(App()->getConfig('RelatedSurveyManagementApiVersion', "0.0"), "0.9" , ">=")) {
    $RelatedSurveysHelper = \RelatedSurveyManagement\RelatedSurveysHelper::getInstance($surveyId);
    return $RelatedSurveysHelper->deleteResponse($oCriteria);
}
return Response::model($this->surveyId)->deleteAll($oCriteria);
```  
## Contribute and issue

Contribution are welcome, for patch and issue : use [gitlab](https://gitlab.com/SondagesPro/managament/RelatedSurveyManagement).

## Home page & Copyright
- HomePage <https://extensions.sondages.pro/rubrique33>
- Copyright © 2018-2019 Denis Chenu <http://sondages.pro>
- Licence : GNU Affero General Public License <https://www.gnu.org/licenses/agpl-3.0.html>
