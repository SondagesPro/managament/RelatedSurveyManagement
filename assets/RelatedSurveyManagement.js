/**
 * Javascript for RelatedSurveyManagement.js
 * @author Denis Chenu <https://sondages.pro
 * @license magnet:?xt=urn:btih:c80d50af7d3db9be66a4d0a86db0286e4fd33292&dn=bsd-3-clause.txt BSD 3 Clause
 * @version 0.0.1-beta3
 */

var RelatedSurveyManagement = {
    init: function (options) {
        this.setHelpers();
        this.setActionUpdateSurvey();
    },
    setHelpers: function () {
        /* Set the tooltips via sr-only */
        $('[data-sr-tooltip]').tooltip({
            html: true,
            title: function () {
                return $(this).find(".sr-only").first().html();
            }
        });
        $("#selectsurvey").select2({ theme: 'bootstrap', width: 'resolve', dropdownParent: $("#set-new-survey-modal")});
    },
    setActionUpdateSurvey: function () {
        $('.set-new-survey-action').on('click', function () {
            $("#selectsurvey-question").val($(this).data("question-id"));
            $('#set-new-survey-modal').modal('show');
            $('#selectsurvey').focus();
        });
        $(document).on("hide.bs.modal", "#set-new-survey-modal", function (e) {
            $("#selectsurvey-question").val("");
            $("#selectsurvey").val("");
        });
    },
};
